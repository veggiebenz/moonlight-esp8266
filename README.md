# Moonlight - NodeMCU / ESP8266 NeoPixel Control over WiFi #

Moonlight is a project demonstrating ESP8266 Wifi connectivity and control of a NeoPixel LED array through ESP Web Server and Amazon Alexa (Echo).

Features:

* Smart Wifi module automatically sets up an Access Point if it can't join your local WiFi.  You can use this mode to add the device to any WiFi network.

* Alexa integration allows control of NeoPixel array without having to create any Alexa Skills - direct access on your local network.

	* Alexa, turn (device) ON
	
	* Alexa, change (device) to BLUE
	
	* Alexa, set (device) brightness to 50%

* Web / Mobile UI allows full control of device 

	* Set RGB color with sliders
	
	* Rainbow mode 
	
	* Accessible using Zeroconf / Bonjour / Avahi = (http://devicename.local)

* Should work with ESP8266 and ESP32

Much thanks to:

* https://github.com/Aircoookie/Espalexa

* https://github.com/me-no-dev/ESPAsyncWebServer

* https://github.com/alanswx/ESPAsyncWiFiManager


I 3D Printed a moon (using ABS) from this guy:

https://www.instructables.com/id/Print-Your-Own-Moon/

Then put the NodeMCU / NeoPixel array inside.  Please note the capacitor is not strictly necessary.

![skeletron.png](https://bitbucket.org/repo/BRbR7a/images/3903586658-skeletron.png)

![nodemcu.png](https://bitbucket.org/repo/BRbR7a/images/1242224607-nodemcu.png)