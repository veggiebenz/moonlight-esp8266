/*
 *  Controls NeoPixel array using a built in web UI, or through Amazon Alexa.  Tested on ESP8266 
 *  and should also be compatible with ESP32
 */
 
#define ESPALEXA_ASYNC //it is important to define this before #include <Espalexa.h>!
#include <Espalexa.h>

#ifdef ARDUINO_ARCH_ESP32
  #include <WiFi.h>
  #include <AsyncTCP.h>
  #include <ESPmDNS.h>
#else
  #include <ESP8266WiFi.h>
  #include <ESPAsyncTCP.h>
  #include <ESP8266mDNS.h>
#endif
#include <ESPAsyncWebServer.h>
#include <ESPAsyncWiFiManager.h> 
#include <NeoPixelBus.h>

#define PixelCount 8    // TODO:  change to 12 for ring neopixel, 8 for strip
#define PixelPin 2  // make sure to set this to the correct pin, ignored for Esp8266

const char* host = "moonlight";

const byte MODE_NORMAL = 0;
const byte MODE_BLINK = 1;
const byte MODE_TIMED = 2;
const byte MODE_RAINBOW = 3;

typedef struct {
    int R;
    int G;
    int B;
} rgb;

rgb rainbow[21];

byte mode;
byte rainbow_step; 
uint8_t R = 0;   // RgbColor wants int, and my sliders have int - why bother with hex?
uint8_t G = 0;
uint8_t B = 0;

//callback functions
void colorLightChanged(uint8_t brightness, uint32_t rgb);

Espalexa espalexa;
AsyncWebServer server(80);
DNSServer dns;
NeoPixelBus<NeoGrbFeature, Neo800KbpsMethod> strip(PixelCount, PixelPin);

void initColor()
{
    float frequency = .3;
    for (int counter = 0; counter <= 21; counter++)
    {
        rainbow[counter].R = sin(frequency * counter + 0) * 127 + 128;
        rainbow[counter].G = sin(frequency * counter + 2) * 127 + 128;
        rainbow[counter].B = sin(frequency * counter + 4) * 127 + 128;
    }
}

void setup()
{
    Serial.begin(115200);
    Serial.println("Starting up...");

    AsyncWiFiManager wifiManager(&server,&dns);
    wifiManager.autoConnect(host);

    strip.Begin();
    strip.Show();

    if (!MDNS.begin(host)) {
        Serial.println("Error setting up MDNS responder!");
        while(1) {
            delay(1000);
        }
    }
    Serial.println("mDNS responder started");

    // Add service to MDNS-SD
    MDNS.addService("http", "tcp", 80);
 
    bool result = SPIFFS.begin();
    if (result != true) { SPIFFS.format();}
    Serial.println("SPIFFS opened: " + result);

    initColor();

    server.serveStatic("/", SPIFFS, "/").setDefaultFile("index.html");

    server.on("/mode", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send ( 200, "text/plain", String(mode) );
    });

    server.on("/mode", HTTP_POST, [](AsyncWebServerRequest *request){
        if (request->hasParam("mode", true)) {
            String temp = request->getParam("mode", true)->value();
            mode = atoi(temp.c_str());
            Serial.print("Setting mode = " );
            Serial.println(mode);
            request->send ( 200, "text/plain", "OK" );
        } else {
            request->send(500, "Set mode failed.");
        }
    });

    server.on("/color", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(200, "text/plain", colorGet() );
    });

    server.on("/color", HTTP_POST, [](AsyncWebServerRequest *request){
        request->send(200, "text/plain", "Setting color from Web POST...");
        String message;
        if (request->hasParam("R", true)) {
            message = request->getParam("R", true)->value();
            R = atoi(message.c_str());
        }
        if (request->hasParam("G", true)) {
            message = request->getParam("G", true)->value();
            G = atoi(message.c_str());
        }
        if (request->hasParam("B", true)) {
            message = request->getParam("B", true)->value();
            B = atoi(message.c_str());
        }
        colorSet(R, G, B);
    });

    server.onNotFound([](AsyncWebServerRequest *request){
        if (!espalexa.handleAlexaApiCall(request)) //if you don't know the URI, ask espalexa whether it is an Alexa control request
        {
            request->send(404, "text/plain", "Not found");          //whatever you want to do with 404s
        }
    });

    // Define your devices here.
    espalexa.addDevice(host, colorLightChanged); //simplest definition, default state off
    espalexa.begin(&server); //give espalexa a pointer to your server object so it can use your server instead of creating its own
}

void doMode()
{
    if (mode == MODE_RAINBOW)
    {
        if (rainbow_step >= 21) {
            rainbow_step = 0;
        }
        RgbColor color = RgbColor(rainbow[rainbow_step].R, rainbow[rainbow_step].G, rainbow[rainbow_step].B);
        rainbow_step ++;
        for (int i=0; i<PixelCount; i++) {
            strip.SetPixelColor(i, color);
        }
        strip.Show();
        delay (100);
    }
}

String colorGet()
{
    String jsonobj = "{ \"R\" : " ;
    jsonobj += R;
    jsonobj += ", \"G\" : ";
    jsonobj += G;
    jsonobj += ", \"B\" : ";
    jsonobj += B;
    jsonobj += " }";
    return jsonobj;
}

void colorSet(uint8_t R, uint8_t G, uint8_t B)
{
    mode = MODE_NORMAL;         // if they selected a color, stop rainbow mode
    RgbColor color = RgbColor(R, G, B);
    for (int i=0; i<PixelCount; i++) {
        strip.SetPixelColor(i, color);
    }
    strip.Show();
}

void loop()
{
    espalexa.loop();
    MDNS.update();
    doMode();
    delay(1);
}

void colorLightChanged(uint8_t brightness, uint32_t rgb)        //the color device callback function has two parameters
{ 
  Serial.print("Brightness: ");
  Serial.println(brightness);
  Serial.print(", Red: ");
  R = (rgb >> 16) & 0xFF;
  R = R * (brightness / 255.0);
  Serial.print(R); //get red component
  Serial.print(", Green: ");
  G = (rgb >>  8) & 0xFF;
  G = G * (brightness / 255.0);
  Serial.print(G); //get green
  Serial.print(", Blue: ");
  B = rgb & 0xFF; 
  B = B * (brightness / 255.0);
  Serial.println(B); //get blue
  colorSet(R,G,B);
}

